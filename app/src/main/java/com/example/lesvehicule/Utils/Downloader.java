package com.example.lesvehicule.Utils;


import com.example.lesvehicule.Object.Vehicule;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

public class Downloader {

    String lien = "http://bit.ly/2tIAaaN";
    public Downloader(){
        // Transformation du lien en URL puis ouverture de la requete.
        try{

            URL url = new URL(lien);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            url.openStream()));

            String inputLine;
            String test = "";

            while ((inputLine = in.readLine()) != null){

                test += inputLine;
            }

            System.out.println(test);
            in.close();
            // Conversion des données au format JSON
            JSONObject jArray = new JSONObject(test);

            // Ciblage de l'information que l'on recherche.
            JSONArray listeVehicule = jArray.getJSONObject("vehicleList").getJSONArray("response");

            // On parcourt de la boucle des vehicules.
            for (int i = 0; i < listeVehicule.length(); i++) {

                JSONObject leVehicule = listeVehicule.getJSONObject(i);
                String icon = leVehicule.getJSONObject("icon").getJSONObject("url").getString("left");

                // Enregistrement de chaque véhicule dans la BDD
                Vehicule v = new Vehicule(leVehicule.getString("id"),leVehicule.getString("name"),icon);
                v.save();

            }

        } catch (Exception e){

            System.out.println("Erreur lors de la connexion au réseau : " + e.getMessage());
        }
    }
}
