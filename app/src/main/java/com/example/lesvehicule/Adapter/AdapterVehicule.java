package com.example.lesvehicule.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lesvehicule.Object.Vehicule;
import com.example.lesvehicule.R;
import com.squareup.picasso.Picasso;

public class AdapterVehicule extends RecyclerView.Adapter<AdapterVehicule.MyHolder> {

    private ViewGroup parent;
    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.activity_cell_vehicule, parent, false);

        this.parent = parent;
        return new MyHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder holder, int position) {

        Vehicule leVehicule = Vehicule.listAll(Vehicule.class).get(position);

        holder.afficher(leVehicule,position);
    }

    @Override
    public int getItemCount() {

        return Vehicule.listAll(Vehicule.class).size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {

        int position;
        TextView tv_name;
        ImageView icon;
        View itemView;

        public MyHolder(View itemView) {

            super(itemView);
            this.itemView = itemView;
            tv_name = itemView.findViewById(R.id.tv_name);
            icon = itemView.findViewById(R.id.imgIcon);
        }

        public void afficher(final Vehicule leVehicule, int position){

            this.position = position;

            tv_name.setText(leVehicule.getName());
            Picasso.with(itemView.getContext()).load(leVehicule.getPicto()).into(icon);

            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    Toast.makeText(itemView.getContext(),"Id du véhicule : " + leVehicule.getLidentifiant(),Toast.LENGTH_LONG).show();
                }
            });
        }
    }


}
