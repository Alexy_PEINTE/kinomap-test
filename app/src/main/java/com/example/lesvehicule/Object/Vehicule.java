package com.example.lesvehicule.Object;

import com.orm.SugarRecord;

public class Vehicule  extends SugarRecord {

    private String identifiant;
    private String name;
    private String picto;

    public Vehicule() {

    }

    public Vehicule(String identifiant, String name, String picto) {

        this.identifiant = identifiant;
        this.name = name;
        this.picto = picto;
    }

    public String getLidentifiant() {

        return identifiant;
    }

    public void setId(String id) {

        this.identifiant = identifiant;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getPicto() {

        return picto;
    }

    public void setPicto(String picto) {

        this.picto = picto;
    }

    @Override
    public String toString() {

        return "Véhicule n° " + id + " est un " + name;
    }
}
