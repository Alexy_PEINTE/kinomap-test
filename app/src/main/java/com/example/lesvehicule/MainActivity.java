package com.example.lesvehicule;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.lesvehicule.Adapter.AdapterVehicule;
import com.example.lesvehicule.Object.Vehicule;
import com.example.lesvehicule.Utils.Downloader;

public class MainActivity extends AppCompatActivity {
    RecyclerView rv_listeVehicule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv_listeVehicule = findViewById(R.id.rv_vehicule);

        // Vérifie si la liste est déjà remplit. Si c'est le cas on la vide. (Base de données locale.
        if (Vehicule.listAll(Vehicule.class).size() > 0){
            Vehicule.deleteAll(Vehicule.class);
        }

        // Cas lors d'une requete HTTP
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //On remplit la BDD locale avec la nouvelle liste.
        new Downloader();

        configurationRecyclerView();
    }

    private void configurationRecyclerView() {

        rv_listeVehicule.setLayoutManager(new LinearLayoutManager(this));
        RecyclerView.Adapter<AdapterVehicule.MyHolder> adapter = new AdapterVehicule();
        rv_listeVehicule.setAdapter(adapter);

    }
}
